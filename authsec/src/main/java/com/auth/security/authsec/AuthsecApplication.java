package com.auth.security.authsec;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.auth.security.authsec.domain.role.Role;
import com.auth.security.authsec.domain.role.RoleRepository;
import com.auth.security.authsec.domain.user.User;
import com.auth.security.authsec.domain.user.UserRepository;

@SpringBootApplication
public class AuthsecApplication {

	public AuthsecApplication() {
		super();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(AuthsecApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(UserRepository userRepository, RoleRepository roleRepository,
			BCryptPasswordEncoder bCryptPasswordEncoder) {
		return (args) -> {
			Role adminRole = roleRepository.findOneByName("ROLE_ADMIN");
			if (Objects.isNull(adminRole)) {
				Role administratorRole = new Role();
				Role userRole = new Role();

				administratorRole.setRoleName("ROLE_ADMIN");
				userRole.setRoleName("ROLE_USER");

				roleRepository.save(administratorRole);
				roleRepository.save(userRole);
			}

			User rootAdmin = userRepository.findOneByUsername("admin");
			if (Objects.isNull(rootAdmin)) {
				rootAdmin = new User();
				rootAdmin.setUsername("admin");
				rootAdmin.setPassword(bCryptPasswordEncoder.encode("admin"));

				Role role = roleRepository.findOneByName("ROLE_ADMIN");
				if (Objects.isNull(role)) {
					throw new Exception();
				}
				List<Role> roles = new ArrayList<>();
				roles.add(role);
				
				rootAdmin.setRoles(roles);
				rootAdmin.setCreationDate(LocalDateTime.now());
				rootAdmin.setEnabled(true);
				rootAdmin.setAccountNonLocked(true);
				rootAdmin.setAccountNonExpired(true);
				rootAdmin.setCredentialsNonExpired(true);
			}
			userRepository.save(rootAdmin);
		};
	}

}

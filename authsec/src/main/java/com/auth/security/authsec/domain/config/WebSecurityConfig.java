package com.auth.security.authsec.domain.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.auth.security.authsec.domain.user.UserService;

@Configuration
@EnableWebSecurity
@EnableOAuth2Sso
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;
	
	@Autowired
	private CustomLogoutSuccessHandler customLogoutSuccessHandler;
	
	public WebSecurityConfig() {
		super();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.userService).passwordEncoder(getBCryptPasswordEncoder());
		
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		AuthenticationEntryPoint authenticationEntryPoint = new AuthenticationEntryPoint() {

			@Override
			public void commence(HttpServletRequest request, HttpServletResponse response,
					AuthenticationException authException) throws IOException, ServletException {

				response.sendRedirect("/login");

			}

		};

		http


				.authorizeRequests()
					.antMatchers("/", "/register", "/native-login", "/logout", "/bootstrap/**", 
								 "/jquery/**", "/fragments/**", "/styles.css", 
								 "/webjars/**")
					.permitAll()
					.anyRequest()
					.authenticated()
				.and()
					.formLogin()
					.loginPage("/native-login")
					.permitAll()
					.usernameParameter("username")
					.passwordParameter("password")
				.and()
					.exceptionHandling()
					.authenticationEntryPoint(authenticationEntryPoint)
				.and()
					.logout().logoutSuccessHandler(customLogoutSuccessHandler)
					.logoutUrl("/login?logout")
					.logoutSuccessUrl("/")
					.permitAll()
				.and()
					.csrf()		// .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
						.disable(); 
	}
	
	@Bean
	public BCryptPasswordEncoder getBCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

}

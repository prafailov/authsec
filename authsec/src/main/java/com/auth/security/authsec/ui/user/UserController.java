package com.auth.security.authsec.ui.user;

import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.auth.security.authsec.domain.config.Errors;
import com.auth.security.authsec.domain.user.UserService;

/**
 * @author Plamen 
 * 	The Controller for the User model. 
 * 	Its responsibility is sending user information to the domain level from the browser and 
 * 	vice versa.
 */
@Controller
public class UserController {

	private UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("/register")
	public String getRegisterPage(Model model) {
		model.addAttribute("userForm", new UserForm());
		return "register";
	}

	@PostMapping("/register")
	public String processUserRegistration(@Valid @ModelAttribute UserForm userForm, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "error";
		}

		userService.registerUser(userForm);

		return "redirect:/";
	}

	@GetMapping("/welcome")
	public String welcomePage() {
		return "welcome";
	}

	@GetMapping("/native-login")
	public String getLoginPage(@RequestParam(required = false) String error, Model model) {
		if (!Objects.isNull(error)) {
			model.addAttribute("error", Errors.INVALID_CREDENTIALS);
		}

		return "native-login";
	}

}

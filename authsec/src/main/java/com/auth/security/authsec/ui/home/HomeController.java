package com.auth.security.authsec.ui.home;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String getHomePage() {

		return "home";
	}

	@GetMapping("/error")
	public String error() {
		return "error";
	}

}

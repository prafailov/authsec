package com.auth.security.authsec.domain.core;

public interface EntityMapper<F, E> {
	
	F mapToForm(E entity);
	
	E mapToEntity(F form);
	
}

package com.auth.security.authsec.ui.role;

import org.springframework.stereotype.Component;

import com.auth.security.authsec.domain.core.EntityMapper;
import com.auth.security.authsec.domain.role.Role;

@Component
public class RoleMapper implements EntityMapper<RoleForm, Role> {

	public RoleMapper() {
		
	}
	
	@Override
	public RoleForm mapToForm(Role entity) {
		return new RoleForm(entity.getAuthority());
	}

	@Override
	public Role mapToEntity(RoleForm form) {
		return new Role(form.getRoleName());
	}

}

package com.auth.security.authsec.domain.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

	@Query("SELECT r FROM Role r WHERE LOWER(r.roleName)=LOWER(:roleName)")
	Role findOneByName(@Param(value = "roleName") String roleName);
	
}

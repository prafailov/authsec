package com.auth.security.authsec.ui.user;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DetailsController {

	@GetMapping("/user")
	@ResponseBody
	public String getUserDetails() {
		Authentication userAuth = SecurityContextHolder.getContext().getAuthentication();
//		OAuth2Authentication auth = new OAuth2Authentication(null, userAuth);
		return userAuth.toString();
	}

}

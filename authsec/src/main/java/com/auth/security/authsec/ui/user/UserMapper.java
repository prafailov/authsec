package com.auth.security.authsec.ui.user;


import org.springframework.stereotype.Component;

import com.auth.security.authsec.domain.core.EntityMapper;
import com.auth.security.authsec.domain.user.User;

@Component
public class UserMapper implements EntityMapper<UserForm, User> {

	public UserMapper() {
		
	}
	
	@Override
	public UserForm mapToForm(User entity) {
		return new UserForm(entity.getUsername(), entity.getPassword());
	}

	@Override
	public User mapToEntity(UserForm form) {
		return new User(form.getUsername(), form.getPassword());
	}

}

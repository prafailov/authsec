package com.auth.security.authsec.ui.role;

public class RoleForm {
	
	private String roleName;
	
	public RoleForm() {
		
	}
	
	public RoleForm(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	
	
}

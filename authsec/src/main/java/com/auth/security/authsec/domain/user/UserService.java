package com.auth.security.authsec.domain.user;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.auth.security.authsec.ui.user.UserForm;

/**
 * @author Plamen
 *	The service for managing domain-level logic with the User model. Extends the UserDetailsService 
 *	which is a specification in Spring Security. Its contract is the loadUserByUsername(username) method, which the 
 *	User service should implement. 
 */
public interface UserService extends UserDetailsService {
	
	/**
	 * Registers a new user.
	 * @param userForm
	 */
	void registerUser(UserForm userForm);
	
}

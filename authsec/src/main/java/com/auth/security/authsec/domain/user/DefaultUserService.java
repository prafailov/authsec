package com.auth.security.authsec.domain.user;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.auth.security.authsec.domain.config.Errors;
import com.auth.security.authsec.domain.role.Role;
import com.auth.security.authsec.domain.role.RoleRepository;
import com.auth.security.authsec.ui.user.UserForm;
import com.auth.security.authsec.ui.user.UserMapper;

/**
 * @author Plamen
 * Concrete implementation of the UserDetailsService interface.
 */
@Service
public class DefaultUserService implements UserService {

	private UserRepository userRepository;
	private RoleRepository roleRepository;
	private UserMapper userMapper;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public DefaultUserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder,
			RoleRepository roleRepository, UserMapper userMapper) {
		
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.userMapper = userMapper;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findOneByUsername(username);

		if (Objects.isNull(user)) {
			throw new UsernameNotFoundException(Errors.INVALID_CREDENTIALS);
		}

		return user;
	}

	@Override
	public void registerUser(UserForm userForm) {
		List<Role> roles = new ArrayList<>();
		User user = new User();
		
		Role role = fetchUserRole("ROLE_USER");
		roles.add(role);
		
		user = userMapper.mapToEntity(userForm);
		String encryptedPassword = bCryptPasswordEncoder.encode(userForm.getPassword());
		setAccountDetails(user, encryptedPassword, roles, true);

		userRepository.save(user);

	}
	
	private final Role fetchUserRole(String roleName) {
		Role role = roleRepository.findOneByName("ROLE_USER");

		if (Objects.isNull(role)) {
			role = new Role("ROLE_USER");
			roleRepository.save(role);
		}
		return role;
	}
	
	private final void setAccountDetails(User user, final String encryptedPassword, List<Role> roles, final boolean setter) {
		user.setCreationDate(LocalDateTime.now());
		user.setRoles(roles);
		
		user.setPassword(encryptedPassword);
		user.setEnabled(setter);
		user.setAccountNonExpired(setter);
		user.setAccountNonLocked(setter);
		user.setCredentialsNonExpired(setter);
	}
	
}

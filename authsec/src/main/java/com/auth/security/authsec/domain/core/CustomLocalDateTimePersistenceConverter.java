package com.auth.security.authsec.domain.core;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Class for converting 'LocalDateTime' objects for persistence
 *         in the database and backwards conversion for displaying in browser.
 * @author Plamen 
 */
@Converter(autoApply = true)
public class CustomLocalDateTimePersistenceConverter implements AttributeConverter<LocalDateTime, Timestamp> {

	@Override
	public Timestamp convertToDatabaseColumn(LocalDateTime attribute) {
		if (Objects.isNull(attribute))
			return null;
		return Timestamp.valueOf(attribute);
	}

	@Override
	public LocalDateTime convertToEntityAttribute(Timestamp dbData) {
		if (Objects.isNull(dbData.toLocalDateTime()))
			return LocalDateTime.now();
		else
			return dbData.toLocalDateTime();
	}

}
